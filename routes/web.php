<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/{any}', 'SpaController@index')->where('any', '.*');
// Route::get('/', function () {
//     return view(
//         'home',
//         [
//             'posts' => App\Post::with('user')->orderBy('posts.updated_at', 'desc')->get(),
//             'tags' => Spatie\Tags\Tag::all(),
//         ]);
// });
// Auth::routes();
