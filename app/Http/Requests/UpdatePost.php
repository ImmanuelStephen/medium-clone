<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'sub_title' => 'required',
            'description' => 'required',
            'tag_names' => 'required|regex:/^[a-zA-Z0-9,]+$/',
        ];
    }

    public function messages()
    {
        return [
            'tags.regex' => 'Tags should contain only alphabets, number and comma',
        ];
    }
}
