<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;

class UserPostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $posts = Post::with('user')
            ->where('posts.user_id', '=', Auth::id())
            ->orderBy('posts.updated_at', 'desc')
            ->get();

        return $posts;
    }
}
