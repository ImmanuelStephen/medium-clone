/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

import router from "./routes";
import { store } from "./store";
import App from "./layouts/App";

window.Form = require('./Form').Form

window.router = router;

axios.interceptors.request.use(
    config => {
        config.headers.Authorization = `Bearer ${store.getters.token}`;
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    function(response) {
        return response;
    },
    function(error) {
        let httpResponse = [200, 422];

        if (httpResponse.includes(error.response.status)) {
            return Promise.reject(error);
        } else if(error.response.status == 401) { 
            router.push('/signin')
        } else {
            router.push({
                name: "apiException",
                params: { message: error.response.data.message }
            });
        }
    }
);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component(
    "post-brief-view",
    require("./components/PostBriefView.vue").default
);

window.ClassicEditor = require("@ckeditor/ckeditor5-build-classic");
window.CKEditor = require("@ckeditor/ckeditor5-vue");

Vue.use(CKEditor);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app",
    router,
    store,
    render: h => h(App)
});
